//
//  PokemonTableViewCell.swift
//  Marvel
//
//  Created by Jose Diaz on 3/1/18.
//  Copyright © 2018 JoDiaz. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {

    @IBOutlet weak var pokeHeightLabel: UILabel!
    @IBOutlet weak var pokeWeightLabel: UILabel!
    @IBOutlet weak var pokeNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillPokemonData (pokemon: Pokemon){
        pokeHeightLabel.text = "\(pokemon.height ?? 0.0)"
        pokeWeightLabel.text = "\(pokemon.weight ?? 0.0)"
        pokeNameLabel.text = pokemon.name
    }
}
