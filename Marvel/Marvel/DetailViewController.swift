//
//  DetailViewController.swift
//  Marvel
//
//  Created by Jose Diaz on 3/1/18.
//  Copyright © 2018 JoDiaz. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var pokemon:Pokemon?
    override func viewDidLoad() {
        super.viewDidLoad()
        print(pokemon?.name)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
