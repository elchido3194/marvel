//
//  ViewController.swift
//  Marvel
//
//  Created by Jose Diaz on 28/11/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //MARK:- OUTLETS
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var alturaLabel: UILabel!
    @IBOutlet weak var anchuraLabel: UILabel!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- ACTIONS
    @IBAction func consultarButtonPressed(_ sender: Any) {
        let URL = "https://pokeapi.co/api/v2/pokemon/1"
        //Tomar en consideracion que el DataResponse<Pokemon> es de la clase pokemon creada en el archivo pokemon.switft
        Alamofire.request(URL).responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            
            DispatchQueue.main.async {
                self.nameLabel.text = pokemon?.name ?? ""
                self.alturaLabel.text = "\(pokemon?.height ?? 0)"
                self.anchuraLabel.text = "\(pokemon?.weight ?? 0)"
            }
        }
    
    }
}

