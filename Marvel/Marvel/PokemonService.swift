//
//  PokemonService.swift
//  Marvel
//
//  Created by Jose Diaz on 2/1/18.
//  Copyright © 2018 JoDiaz. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol PokemonServiceDelegate {
    func get20FirstPokemons(pokemons:[Pokemon])
}

class PokemonService {
    var delegate:PokemonServiceDelegate?
    func downloadPokemons(){
        var pokemonArray:[Pokemon] = []
        let dpGR = DispatchGroup()
        for i in 1...20 {
            dpGR.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response:DataResponse<Pokemon>) in
                let pokemon = response.result.value
                print(pokemon?.name)
                pokemonArray.append(pokemon!)
                //print(pokemonArray)
                //Utilizamos un delegate para poder enviar los datos de los primeros 20 pokemones
                dpGR.leave()
            }
        }
        dpGR.notify(queue: .main) {
            let sortedArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
            self.delegate?.get20FirstPokemons(pokemons: sortedArray)
        }
    }
}
