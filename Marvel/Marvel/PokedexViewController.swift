//
//  PokedexViewController.swift
//  Marvel
//
//  Created by Jose Diaz on 29/11/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {

    var pokemonArray:[Pokemon] = []
    var pokemonSelected:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        let service = PokemonService()
        service.delegate = self
        service.downloadPokemons()

        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var tableViewOutlet: UITableView!
    
    func get20FirstPokemons(pokemons: [Pokemon]){
        //print(pokemons)
        pokemonArray = pokemons
        tableViewOutlet.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 0:
//            return 5
//        default:
//            return 10
//        }
        return pokemonArray.count
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        pokemonSelected = indexPath.row
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        cell.fillPokemonData(pokemon: pokemonArray[indexPath.row])
        //cell.textLabel?.text = pokemonArray[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Seccion 1"
        default:
            return "Seccion 2"
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemonDetails = segue.destination as! DetailViewController
        pokemonDetails.pokemon = pokemonArray[pokemonSelected!]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
